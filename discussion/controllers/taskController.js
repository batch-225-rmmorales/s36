// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Task = require("../models/task");

module.exports.getAllTask = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Task.find({});
};

module.exports.newTask = async (req) => {
  let newTask = new Task({
    name: req.body.name,
    age: req.body.age,
    status: req.body.status,
  });

  return await newTask.save();
};

module.exports.deleteByName = async (req) => {
  let toDelete = await Task.findOne({ name: req.body.name });
  return await toDelete.remove();
};

module.exports.deleteByID = async (taskID) => {
  let toDelete = await Task.findOne({ _id: taskID });
  return await toDelete.remove();
};

module.exports.updateTask = async (req) => {
  let toUpdate = await Task.findOne(req.body.filter);
  return await toUpdate.update(req.body.update);
  //   return await Task.findOneAndUpdate(req.filter, req.update, {
  //     returnDocument: "after",
  //   });
};

module.exports.updateByID = async (taskID, req) => {
  let toUpdate = await Task.findOne({ _id: taskID });
  console.log(req.body.update);
  await toUpdate.update(req.body.update);
  return toUpdate.save();
};
