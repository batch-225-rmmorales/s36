// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

const Task = require("../models/task");

module.exports.getAllTask = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Task.find({});
};

module.exports.newTask = async () => {
  // return statement. returns the result of the mongoose method
  //the then method is used to wait for the mongoose method to finish before sending the result back to the route.
  return await Task.findOne({ name: req.body.name }, (err, result) => {
    // If a document was found and the document's name matches the information sent via the client/Postman
    if (result != null && result.name == req.body.name) {
      // Return a message to the client/Postman
      return res.send("Duplicate task found");

      // If no document was found
    } else {
      // Create a new task and save it to the database
      let newTask = new Task({
        name: req.body.name,
      });

      // The "save" method will store the information to the database
      // Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
      // The "save" method will accept a callback function which stores any errors found in the first parameter
      // The second parameter of the callback function will store the newly saved document
      // Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
      newTask.save((saveErr, savedTask) => {
        // If there are errors in saving
        if (saveErr) {
          // Will print any errors found in the console
          // saveErr is an error object that will contain details about the error
          // Errors normally come as an object data type
          return console.error(saveErr);

          // No error found while creating the document
        } else {
          // Return a status code of 201 for created
          // Sends a message "New task created" on successful creation
          return res.status(201).send("New task created");
        }
      });
    }
  });
};
