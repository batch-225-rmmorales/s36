//create the schema,  model and export the file

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  name: String,
  age: Number,
  status: {
    type: String,
    default: "pending",
  },
});

// module.exports is the way for node js to treat this value as package
module.exports = mongoose.model("Task", taskSchema);
