const express = require("express");
const taskController = require("../controllers/taskController");
//create a router instance that functions as middle ware and routing system
// allows access to http method middlewares taht makes it e4asier to creat routes
const router = express.Router();

// section router
router.get("/", async (res, req) => {
  resultFromController = await taskController.getAllTask();
  res.send(resultFromController);
});

module.exports = router;
