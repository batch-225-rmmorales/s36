const express = require("express");
const taskController = require("../controllers/taskController");
//create a router instance that functions as middle ware and routing system
// allows access to http method middlewares taht makes it e4asier to creat routes
const router = express.Router();

// section router
router.get("/", async (req, res) => {
  console.log("im in get/");
  resultFromController = await taskController.getAllTask();
  res.send(resultFromController);
});

router.get("/:id", async (req, res) => {
  console.log("im in /:id");
  resultFromController = await taskController.getTask(req.params.id);
  res.send(resultFromController);
});
router.put("/:id/complete", async (req, res) => {
  console.log("im in /:id");
  resultFromController = await taskController.completeTask(req.params.id);
  res.send(resultFromController);
});

router.post("/", async (req, res) => {
  resultFromController = await taskController.newTask(req);
  res.send(resultFromController);
});

router.delete("/", async (req, res) => {
  resultFromController = await taskController.deleteByName(req);
  res.send(resultFromController);
});

router.delete("/delete/:id", async (req, res) => {
  resultFromController = await taskController.deleteByID(req.params.id);
  res.send(resultFromController);
});

router.put("/", async (req, res) => {
  resultFromController = await taskController.updateTask(req);
  res.send(resultFromController);
});

router.put("/update/:id", async (req, res) => {
  resultFromController = await taskController.updateByID(req.params.id, req);
  res.send(resultFromController);
});

module.exports = router;
